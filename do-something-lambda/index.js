// eslint-disable-next-line strict
'use strict';

const AWS = require('aws-sdk');
const { DateTime } = require('luxon');
const { Client: PgClient } = require('pg');
const SQL = require('sql-template-strings');

const s3 = new AWS.S3({
    apiVersion: '2006-03-01',
});

const secretsManager = new AWS.SecretsManager({
    apiVersion: '2017-10-17',
});

const insertQuery = data => SQL`
INSERT INTO "data"
  (
    time_stamp,
    "data"
  )
VALUES
  (
    ${data.timeStamp},
    ${data.data}
  )
`;

module.exports.handler = async event => {
    // Get the database credentials from secrets manager
    const databaseCredentials = await secretsManager
        .getSecretValue({ SecretId: 'databaseCredentials' })
        .promise()
        .then(response => JSON.parse(response.SecretString));

    const timeStamp = DateTime.utc().toISO({ suppressMilliseconds: true });

    // Push something to s3
    await s3
        .upload({
            Bucket: process.env.S3_BUCKET,
            Key: `Data_${timeStamp}.json`,
            Body: Buffer.from(event.data),
        })
        .promise();

    // Write something to a database
    const postgres = new PgClient(databaseCredentials);

    await postgres.connect();

    await postgres.query(
        insertQuery({
            data: event.data,
            timeStamp,
        })
    );

    await postgres.end();

    // Return a 200 as everything is ok
    return {
        statusCode: 200,
    };
};
