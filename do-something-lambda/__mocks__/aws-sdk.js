const originalModule = jest.requireActual('aws-sdk');

const credentials = {
    accessKeyId: 'a',
    secretAccessKey: 'b',
    region: 'us-east-1',
};

class S3 {
    constructor(options) {
        const newOptions = {
            ...options,
            signatureVersion: 'v4',
            s3DisableBodySigning: true,
            s3ForcePathStyle: true,
            endpoint: `http://localhost:4572/`,
            credentials,
            region: 'us-east-1',
        };

        return new originalModule.S3(newOptions);
    }
}

class SecretsManager {
    constructor(options) {
        const newOptions = {
            ...options,
            ...credentials,
            endpoint: `http://localhost:4584/`,
        };

        return new originalModule.SecretsManager(newOptions);
    }
}

module.exports = {
    S3,
    SecretsManager,
};
