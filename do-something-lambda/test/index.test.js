require('dotenv').config();

const AWS = require('aws-sdk');
const { Client: PgClient } = require('pg');
const SQL = require('sql-template-strings');

const { handler } = require('../index');

const s3 = new AWS.S3({
    apiVersion: '2006-03-01',
});

const secretsManager = new AWS.SecretsManager({
    apiVersion: '2017-10-17',
});

const postgres = new PgClient({
    database: process.env.POSTGRES_DB,
    host: process.env.POSTGRES_HOST,
    password: process.env.POSTGRES_PASSWORD,
    user: process.env.POSTGRES_USER,
});

const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        // eslint-disable-next-line no-await-in-loop
        await callback(array[index], index, array);
    }
};

describe('handler', () => {
    beforeAll(async () => {
        await postgres.connect();

        // Tear down and setup DB
        try {
            await postgres.query(SQL`DROP TABLE public."data"`);
        } catch {
            // ignore if it is not there
        }

        await postgres.query(
            `CREATE TABLE "data" (
                "id" SERIAL PRIMARY KEY,
                "data" json NOT NULL,
                "time_stamp" TIMESTAMP WITH TIME ZONE NOT NULL
            )`
        );

        // Tear down and setup secretsManager
        try {
            await secretsManager
                .deleteSecret({
                    SecretId: process.env.DATABASE_CREDENTIALS_KEY,
                    ForceDeleteWithoutRecovery: true,
                })
                .promise();
        } catch {
            // ignore if it is not there
        }

        await secretsManager
            .createSecret({
                Name: process.env.DATABASE_CREDENTIALS_KEY,
                SecretString: JSON.stringify({
                    database: process.env.POSTGRES_DB,
                    host: process.env.POSTGRES_HOST,
                    password: process.env.POSTGRES_PASSWORD,
                    user: process.env.POSTGRES_USER,
                }),
            })
            .promise();

        // Tear down and setup s3
        try {
            await s3.headBucket({ Bucket: process.env.S3_BUCKET }).promise();

            const { Contents } = await s3
                .listObjects({ Bucket: process.env.S3_BUCKET })
                .promise();

            asyncForEach(Contents, async item => {
                await s3
                    .deleteObject({
                        Bucket: process.env.S3_BUCKET,
                        Key: item.Key,
                    })
                    .promise();
            });

            await s3.deleteBucket({ Bucket: process.env.S3_BUCKET }).promise();
        } catch {
            // ignore if it is not there
        }

        await s3
            .createBucket({
                Bucket: process.env.S3_BUCKET,
                ACL: 'public-read', // You would not do this in production!
            })
            .promise();
    });

    beforeEach(async () => {
        await postgres.query(SQL`DELETE FROM "data" WHERE 1=1`);
    });

    afterAll(async () => {
        await postgres.end();
    });

    it('should handle some data', async () => {
        const testData = {
            name: 'bob',
        };

        await handler({ data: JSON.stringify(testData) });

        // We can no assert stuff on the database and in S3 :)
    });
});
