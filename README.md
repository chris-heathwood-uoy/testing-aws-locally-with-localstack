# Testing locally with localstack

## Setup

To get up and running install with:

```
npm install
```

And then spin up docker with, you may need to `mkdir -p /tmp/localstack`:

```
docker-compose up
```

Now you should be able to run:

```
npm test
```

You can see what the test has done by checking s3 http://localhost:4572/testBucket and PostgreSQL.

## Teardown

Just Ctrl+C the docker-compose and run:

```
docker-compose down
```

## What is happening?

There is a lambda with code in [do-something-lambda/index.js](https://bitbucket.org/chris-heathwood-uoy/testing-aws-locally-with-localstack/src/master/do-something-lambda/index.js) this is fairly simple and takes in an event and stores it in s3 with a time
stamp and also puts some data in postgres.

When you run the tests these are done using jest (a JS test framework) with a "mocked" AWS (in (do-something-lambda/\_\_mocks\_\_/aws-sdk.js)[https://bitbucket.org/chris-heathwood-uoy/testing-aws-locally-with-localstack/src/master/do-something-lambda/__mocks__/aws-sdk.js])
this just makes AWS use the localstack version of the services we are using (in this case S3 and SecretsManager).

This can then be used locally for tunning tests but the localstack can also be used for development.
